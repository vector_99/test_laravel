<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
})->name('home');

Route::middleware(['auth', 'isAdmin'])->group(function () {
    Route::get('/admin', [App\Http\Controllers\HomeController::class, 'index'])->name('adminHome');
    Route::resource('/admin/brand', 'App\Http\Controllers\BrandController');
    Route::resource('/admin/model', 'App\Http\Controllers\AutomobileModelController');
    Route::resource('/admin/car', 'App\Http\Controllers\AutomobileController');
    Route::resource('/admin/color', 'App\Http\Controllers\ColorController');
});

Auth::routes();
