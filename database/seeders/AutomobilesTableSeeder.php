<?php

namespace Database\Seeders;

use App\Models\Automobile;
use Illuminate\Database\Seeder;

class AutomobilesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Automobile::factory()->count(3)->create();
    }
}
