<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Str;

class UsersTableSeeder extends Seeder
{
    /**
     * Predefined fictitious users
     *
     * @var string[][]
     */
    private $users = [
        [
            'name' => 'John Connor',
            'type' => 'admin',
            'password' => 'p@ssworD',
            'email' => 'john-connor@gmail.com'
        ],
        [
            'name' => 'Sarah Connor',
            'type' => 'user',
            'password' => 'p@ssworD',
            'email' => 'sarah-connor@gmail.com'
        ],
    ];

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        foreach ($this->users AS $user){
            User::factory()->create([
                'name' => $user['name'],
                'password' => bcrypt($user['password']),
                'email' => $user['email'],
                'user_type' => $user['type'],
                'email_verified_at' => now(),
                'remember_token' => Str::random(10),
            ]);
        }


    }
}
