<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use \App\Models\Transmission;

class CreateAutomobilesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('automobiles', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('model_id')->nullable(false)->index('idx_automobile_model_id');
            $table->unsignedBigInteger('color_id')->nullable(false)->index('idx_automobile_color_id');
            $table->string('photo', '100')->nullable(true);
            $table->year('year_manufacture')->nullable(false);
            $table->string('license_plate', '10')->nullable(true);
            $table->double('rental_price')->nullable(false);
            $table->enum('transmission', Transmission::getTransmissionType());
            $table->foreign('model_id')->references('id')->on('automobile_models')->cascadeOnDelete();
            $table->foreign('color_id')->references('id')->on('colors')->restrictOnDelete();
            $table->unique(['model_id','color_id','year_manufacture','license_plate','transmission'],'idx_unique_car');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('automobiles');
    }
}
