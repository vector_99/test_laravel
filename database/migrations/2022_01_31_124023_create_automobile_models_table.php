<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAutomobileModelsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('automobile_models', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('brand_id')->nullable(false)->index('idx_automobile_model_brand_id');
            $table->string('name', 50)->nullable(false)->unique('idx_automobile_model');
            $table->foreign('brand_id')->references('id')->on('brands')->cascadeOnDelete();
            $table->unique(['brand_id','name'],'idx_unique_model');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('automobile_models');
    }
}
