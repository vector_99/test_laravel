<?php

namespace Database\Factories;

use App\Models\AutomobileModel;
use App\Models\Color;
use App\Models\Transmission;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Facades\Storage;

class AutomobileFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition(): array
    {
        return [
            'model_id' => AutomobileModel::factory(),
            'color_id' => Color::factory(),
            'year_manufacture' => $this->faker->year(),
            'license_plate' => $this->faker->lexify('?')
                . $this->faker->numberBetween(100,999)
                . $this->faker->lexify('??') . " " . $this->faker->numberBetween(1,999),
            'rental_price' => $this->faker->randomFloat(2,100,500),
            'transmission' => $this->faker->randomElement(Transmission::getTransmissionType()),
        ];
    }
}
