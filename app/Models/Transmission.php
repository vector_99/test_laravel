<?php

namespace App\Models;

class Transmission
{
    /**
     * @var array
     */
    private const TRANSMISSION_LIST = [
        'Automatic transmission',
        'Mechanical transmission',
        'CVT transmission',
        'Robotic transmission'
    ];

    /**
     * @return string[]
     */
    public static function getTransmissionType(): array
    {
        return self::TRANSMISSION_LIST;
    }

}
