<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Support\Facades\Storage;

class Automobile extends Model
{
    use HasFactory;

    public $timestamps = false;

    /**
     * The number of models to return for pagination.
     *
     * @var int
     */
    protected $perPage = 10;

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'name',
        'model_id',
        'color_id',
        'photo',
        'year_manufacture',
        'license_plate',
        'rental_price',
        'transmission'
    ];

    public function model(): BelongsTo
    {
        return $this->belongsTo(AutomobileModel::class, 'model_id');
    }

    public function color(): HasOne
    {
        return $this->HasOne(Color::class, 'id', 'color_id');
    }

    /**
     * Get the car's name.
     *
     * @return string
     */
    public function getCarNameAttribute(): string
    {
        return "{$this->model->brand->name} {$this->model->name}";
    }

    /**
     * The method deletes a previously uploaded photo
     *
     */
    public function deleteImage(): bool
    {
        if (Storage::disk('public')->exists($this->photo)){
            return Storage::disk('public')->delete($this->photo);
        }
        return true;
    }

}
