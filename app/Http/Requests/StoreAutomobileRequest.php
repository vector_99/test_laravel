<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;

class StoreAutomobileRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        if (Auth::user()->user_type == 'admin') {
            return true;
        }
        return false;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'model_id' => 'required|not_in:0|unique_with:automobiles,model_id,color_id,year_manufacture,license_plate,transmission',
            'color_id' => 'required|not_in:0|integer',
            'year_manufacture' => 'required|integer',
            'rental_price' => 'required|regex:/^\d+(\.\d{1,2})?$/',
            'transmission' => 'required|not_in:0',
            'photo' => 'mimes:jpeg,jpg,png,gif|max:1024',
        ];
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages(): array
    {
        return [
            'model_id.not_in' => 'Please select a model.',
            'model_id.unique_with' => 'This car combination is already in the database.',
            'year_manufacture.required' => 'Year of manufacture must be specified.',
            'year_manufacture.integer' => 'The year must be specified as YEAR. For example 1995.',
            'color_id.not_in' => 'Please select a color.',
            'color_id.required' => 'Color must be specified.'
        ];
    }
}
