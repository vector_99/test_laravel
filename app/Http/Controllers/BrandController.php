<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreBrandRequest;
use App\Http\Requests\UpdateBrandRequest;
use App\Models\Automobile;
use App\Models\Brand;
use Illuminate\Contracts\Support\Renderable;
use Illuminate\Database\QueryException;
use Illuminate\Http\RedirectResponse;

class BrandController extends Controller
{
    private $title = "Brands";

    private $pageName = "";

    /**
     * Display a listing of the resource.
     *
     * @return Renderable
     */
    public function index(): Renderable
    {
        return view('admin/brand/index', [
            'title' => $this->title,
            'brands' => Brand::paginate()
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Renderable
     */
    public function create(): Renderable
    {
        $this->pageName = "Create brand";

        return view('admin.brand.create', [
            'title' => $this->title,
            'pageName' => $this->pageName,
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param StoreBrandRequest $request
     * @return RedirectResponse
     */
    public function store(StoreBrandRequest $request): RedirectResponse
    {
        try {
            Brand::create($request->all());
        } catch (QueryException $error) {
            return redirect()->back()
                ->withErrors(['error' => $error->getMessage()])
                ->withInput($request->input());
        }

        return redirect()->route('brand.index')->with('success','Brand created successfully.');
    }

    /**
     * Display the specified resource.
     *
     * @param Brand $brand
     * @return Renderable
     */
    public function show(Brand $brand): Renderable
    {
        $this->pageName = "View brand \"" . $brand->name . "\"";

        return view('admin.brand.show', [
            'title' => $this->title,
            'pageName' => $this->pageName,
            'brand' => $brand
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param Brand $brand
     * @return Renderable
     */
    public function edit(Brand $brand): Renderable
    {
        $this->pageName = "Edit brand \"" . $brand->name . "\"";

        return view('admin.brand.edit', [
            'title' => $this->title,
            'pageName' => $this->pageName,
            'brand' => $brand
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param UpdateBrandRequest $request
     * @param Brand $brand
     * @return RedirectResponse
     */
    public function update(UpdateBrandRequest $request, Brand $brand): RedirectResponse
    {
        try {
            $brand->update($request->all());
        } catch (QueryException $error) {
            return redirect()->back()
                ->withErrors(['error' => $error->getMessage()])
                ->withInput($request->input());
        }

        return redirect()->route('brand.index')->with('success','Brand updated successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Brand $brand
     * @return RedirectResponse
     */
    public function destroy(Brand $brand): RedirectResponse
    {
        try {
            $brand->delete();
        } catch (QueryException $error) {
            return redirect()->back()
                ->withErrors(['error' => $error->getMessage()]);
        }

        return redirect()->route('brand.index')->with('success','Brand deleted successfully');
    }
}
