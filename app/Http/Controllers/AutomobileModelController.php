<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreAutomobileModelRequest;
use App\Http\Requests\UpdateAutomobileModelRequest;
use App\Models\AutomobileModel;
use App\Models\Brand;
use Illuminate\Contracts\Support\Renderable;
use Illuminate\Database\QueryException;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;

class AutomobileModelController extends Controller
{
    private $title = "Automobile Models";

    private $pageName = "";

    /**
     * Display a listing of the resource.
     *
     * @return Renderable
     */
    public function index(): Renderable
    {
        return view('admin/model/index', [
            'title' => $this->title,
            'models' => AutomobileModel::paginate()
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Renderable
     */
    public function create(): Renderable
    {
        $this->pageName = "Create model";

        return view('admin/model/create', [
            'title' => $this->title,
            'pageName' => $this->pageName,
            'data' => ['brands' => Brand::all(), 'model' => null]
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param StoreAutomobileModelRequest $request
     * @return RedirectResponse
     */
    public function store(StoreAutomobileModelRequest $request): RedirectResponse
    {
        try {
            AutomobileModel::create($request->all());
        } catch (QueryException $error) {
            return redirect()->back()
                ->withErrors(['error' => $error->getMessage()])
                ->withInput($request->input());
        }

        return redirect()->route('model.index')->with('success','Model created successfully.');
    }

    /**
     * Display the specified resource.
     *
     * @param AutomobileModel $model
     * @return Renderable
     */
    public function show(AutomobileModel $model): Renderable
    {
        $this->pageName = "View model \"" . $model->full_model_name . "\"";

        return view('admin.model.show', [
            'title' => $this->title,
            'pageName' => $this->pageName,
            'model' => $model
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param AutomobileModel $model
     * @return Renderable
     */
    public function edit(AutomobileModel $model): Renderable
    {
        $this->pageName = "Edit model \"" . $model->full_model_name . "\"";

        return view('admin.model.edit', [
            'title' => $this->title,
            'pageName' => $this->pageName,
            'data' => ['brands' => Brand::all(), 'model' => $model]
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param UpdateAutomobileModelRequest $request
     * @param AutomobileModel $model
     * @return RedirectResponse
     */
    public function update(UpdateAutomobileModelRequest $request, AutomobileModel $model): RedirectResponse
    {
        try {
            $model->update($request->all());
        } catch (QueryException $error) {
            return redirect()->back()
                ->withErrors(['error' => $error->getMessage()])
                ->withInput($request->input());
        }

        return redirect()->route('model.index')
                ->with('success','Model updated successfully.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param AutomobileModel $model
     * @return RedirectResponse
     */
    public function destroy(AutomobileModel $model): RedirectResponse
    {
        try {
            $model->delete();
        } catch (QueryException $error) {
            return redirect()->back()
                ->withErrors(['error' => $error->getMessage()]);
        }

        return redirect()->route('model.index')
            ->with('success','Model deleted successfully.');
    }
}
