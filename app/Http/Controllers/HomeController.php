<?php

namespace App\Http\Controllers;

use App\Models\Automobile;
use App\Models\AutomobileModel;
use App\Models\Brand;
use Illuminate\Contracts\Support\Renderable;

class HomeController extends Controller
{
    /**
     * Show the application dashboard.
     *
     * @return Renderable
     */
    public function index(): Renderable
    {
        return view('admin/home', [
            'totalBrands' => Brand::all()->count(),
            'totalModels' => AutomobileModel::all()->count(),
            'totalAutomobile' => Automobile::all()->count()
        ]);
    }
}
