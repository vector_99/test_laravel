<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreColorRequest;
use App\Http\Requests\UpdateColorRequest;
use App\Models\Automobile;
use App\Models\Color;
use Illuminate\Contracts\Support\Renderable;
use Illuminate\Database\QueryException;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Response;

class ColorController extends Controller
{
    private $title = "Colors";

    private $pageName = "";

    /**
     * Display a listing of the resource.
     *
     * @return Renderable
     */
    public function index(): Renderable
    {
        return view('admin/color/index', [
            'title' => $this->title,
            'colors' => Color::paginate()
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Renderable
     */
    public function create(): Renderable
    {
        $this->pageName = "Create color";

        return view('admin.color.create', [
            'title' => $this->title,
            'pageName' => $this->pageName,
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param StoreColorRequest $request
     * @return RedirectResponse
     */
    public function store(StoreColorRequest $request): RedirectResponse
    {
        try {
            Color::create($request->all());
        } catch (QueryException $error) {
            return redirect()->back()
                ->withErrors(['error' => $error->getMessage()])
                ->withInput($request->input());
        }

        return redirect()->route('color.index')->with('success','Color created successfully.');
    }

    /**
     * Display the specified resource.
     *
     * @param Color $color
     * @return Renderable
     */
    public function show(Color $color): Renderable
    {
        $this->pageName = "View color \"" . $color->name . "\"";

        return view('admin.color.show', [
            'title' => $this->title,
            'pageName' => $this->pageName,
            'color' => $color
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param Color $color
     * @return Renderable
     */
    public function edit(Color $color): Renderable
    {
        $this->pageName = "Edit brand \"" . $color->name . "\"";

        return view('admin.color.edit', [
            'title' => $this->title,
            'pageName' => $this->pageName,
            'color' => $color
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param UpdateColorRequest $request
     * @param Color $color
     * @return RedirectResponse
     */
    public function update(UpdateColorRequest $request, Color $color): RedirectResponse
    {
        try {
            $color->update($request->all());
        } catch (QueryException $error) {
            return redirect()->back()
                ->withErrors(['error' => $error->getMessage()])
                ->withInput($request->input());
        }

        return redirect()->route('color.index')->with('success','Color updated successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Color $color
     * @return RedirectResponse
     */
    public function destroy(Color $color): RedirectResponse
    {
        try {
            $color->delete();
        } catch (QueryException $error) {
            return redirect()->back()
                ->withErrors(['error' => $error->getMessage()]);
        }

        return redirect()->route('color.index')
            ->with('success','Color deleted successfully.');
    }
}
