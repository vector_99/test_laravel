<?php

namespace App\Http\Controllers;

use App\Helpers\Enum;
use App\Http\Requests\StoreAutomobileRequest;
use App\Http\Requests\UpdateAutomobileRequest;
use App\Models\Automobile;
use App\Models\AutomobileModel;
use App\Models\Brand;
use App\Models\Color;
use Illuminate\Contracts\Support\Renderable;
use Illuminate\Database\QueryException;
use Illuminate\Http\RedirectResponse;
use Illuminate\Support\Facades\Storage;

class AutomobileController extends Controller
{
    /**
     * @var string
     */
    private $title = "Cars";

    /**
     * @var string
     */
    private $pageName = "";

    /**
     * Display a listing of the resource.
     *
     * @return Renderable ;
     */
    public function index(): Renderable
    {
        return view('admin/automobile/index', [
            'title' => $this->title,
            'cars' => Automobile::paginate()
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Renderable
     */
    public function create(): Renderable
    {
        $this->pageName = "Create car";

        return view('admin/automobile/create', [
            'title' => $this->title,
            'pageName' => $this->pageName,
            'data' => [
                'brands' => Brand::all(),
                'models' => AutomobileModel::all(),
                'transmissions' => Enum::getEnumValues('automobiles', 'transmission'),
                'colors' => Color::all(),
                'car' => null
            ]
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param StoreAutomobileRequest $request
     * @return RedirectResponse
     */
    public function store(StoreAutomobileRequest $request): RedirectResponse
    {
        if ($request->hasFile('car_photo')){
            $request->merge([
                'photo' => Storage::disk('public')->put('photo', $request->file('car_photo'))
            ]);
        }

        try {
            Automobile::create($request->except('car_photo'));
        } catch (QueryException $error) {
            return redirect()->back()
                ->withErrors(['error' => $error->getMessage()])
                ->withInput($request->input());
        }

        return redirect()->route('car.index')
                ->with('success','Car created successfully.');
    }

    /**
     * Display the specified resource.
     *
     * @param Automobile $car
     * @return Renderable
     */
    public function show(Automobile $car): Renderable
    {
        $this->pageName = "View car \"" . $car->car_name . "\"";

        return view('admin.automobile.show', [
            'title' => $this->title,
            'pageName' => $this->pageName,
            'car' => $car
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param Automobile $car
     * @return Renderable
     */
    public function edit(Automobile $car): Renderable
    {
        $this->pageName = "Edit car \"" . $car->car_name . "\"";

        return view('admin.automobile.edit', [
            'title' => 'Edit brand',
            'pageName' => $this->pageName,
            'data' => [
                'brands' => Brand::all(),
                'models' => AutomobileModel::all(),
                'transmissions' => Enum::getEnumValues('automobiles', 'transmission'),
                'colors' => Color::all(),
                'car' => $car]
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param UpdateAutomobileRequest $request
     * @param Automobile $car
     * @return RedirectResponse
     */
    public function update(UpdateAutomobileRequest $request, Automobile $car): RedirectResponse
    {
        if ($request->hasFile('car_photo')){
            $car->deleteImage();
            $request->merge([
                'photo' => Storage::disk('public')->put('photo', $request->file('car_photo'))
            ]);
        }

        try {
            $car->update($request->except('car_photo'));
        } catch (QueryException $error) {
            return redirect()->back()
                ->withErrors(['error' => $error->getMessage()])
                ->withInput($request->input());
        }

        return redirect()->route('car.index')
                ->with('success','Car updated successfully.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Automobile $car
     * @return RedirectResponse
     */
    public function destroy(Automobile $car): RedirectResponse
    {
        try {
            $car->delete();
            $car->deleteImage();
        } catch (QueryException $error) {
            return redirect()->back()
                ->withErrors(['error' => $error->getMessage()]);
        }

        return redirect()->route('car.index')
                ->with('success','Car deleted successfully.');
    }
}
