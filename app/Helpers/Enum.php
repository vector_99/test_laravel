<?php
/**
 * Enum helper class
 * @author Anton Khomenko <vector.xoma@gmail.com>
 */
namespace App\Helpers;

use Exception;
use Illuminate\Support\Facades\DB;

class Enum
{
    /**
     * Getting Enum values from $column of $table
     *
     * @param string|null $table
     * @param string|null $column
     * @return array|Exception
     */
    public static function getEnumValues(string $table = null, string $column = null)
    {
        $enum = [];

        if ($table && $column){
            $type = DB::select(DB::raw("SHOW COLUMNS FROM {$table} WHERE Field = '{$column}'"));

            if (empty($type)){
                return $enum;
            }

            $type = $type[0]->Type;

            preg_match('/^enum((.*))$/', $type, $matches);

            if (!empty($matches)){
                $search  = array('(', ')', '\'');
                $replace = array('', '', '');
                $values = str_replace($search, $replace, $matches[1]);

                foreach(explode(',', $values) as $value)
                {
                    $enum[] = $value;
                }
            }
            else{
                return $enum;
            }
        }

        return $enum;
    }
}
