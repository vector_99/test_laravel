@extends('layouts.app')

@section('content')
    <section class="content-header">
        @include('admin.includes.headerWithBreadcrumbs', [
            'route' => route('brand.index'),
            'title' => $title,
            'pageName' => $pageName ?? null
        ])
    </section>
    <section class="content">
        <div class="card">
            <div class="card-header">
                <a class="btn btn-success btn-sm mw-100" href="{{ route('brand.edit', $brand->id) }}">
                    <i class="fas fa-pencil-alt"></i>
                </a>
            </div>
            <div class="card-body">
                <div class="form-group">
                    <label for="exampleInputEmail1">Brand name</label>
                    <h3>{{ $brand->name }}</h3>
                </div>
            </div>
        </div>
    </section>
@endsection
