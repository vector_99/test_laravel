@extends('layouts.app')

@section('content')
    <section class="content-header">
        @include('admin.includes.headerWithBreadcrumbs', [
            'route' => route('color.index'),
            'title' => $title,
            'pageName' => $pageName ?? null
        ])
    </section>
    <section class="content">
        @if(session()->has('success'))
            @include('admin.includes.success')
        @endif
        @if ($errors->any())
            @include('admin.includes.errors', ['errors' => $errors])
        @endif
        <div class="card">
            <div class="card-header">
                <a class="btn btn-success btn-sm mw-100" href="{{ route('color.create') }}">
                    <i class="far fa-plus-square"></i>
                </a>
                <div class="card-tools">
                    {{ $colors->links() }}
                </div>
            </div>
            @if ($colors->isNotEmpty())
            <div class="card-body p-0" style="display: block;">
                <table class="table table-striped projects">
                    <thead>
                    <tr>
                        <th style="width: 1%">#</th>
                        <th style="width: 40%">Color Name</th>
                        <th style="width: 20%">{{-- --}}</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($colors AS $color)
                    <tr>
                        <td>
                            {{ $color->id }}
                        </td>
                        <td>
                            <a href="{{ route('color.show', $color->id) }}">
                                {{ $color->name }}
                            </a>
                        </td>
                        <td class="project-actions text-right">
                            <div class="btn-group">
                                <a class="btn btn-outline-primary btn-sm" href="{{ route('color.edit', $color->id) }}">
                                    <i class="fas fa-pencil-alt pt-2 pl-1 pr-1"></i>
                                </a>
                                <a type="button" class="btn btn-outline-danger delete-trigger" data-toggle="modal"
                                        data-target="#modal_delete"
                                        data-delete-url="{{ route('color.destroy', $color->id) }}">
                                    <i class="fas fa-trash"></i>
                                </a>
                            </div>
                        </td>
                    </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
            @else
                <div class="card-body" style="display: block;">
                    <div class="col-12">
                        <h5>{{ __('messages.colors.noDataFound') }}</h5>
                    </div>
                </div>
            @endif

            <!-- /.card-body -->
            <div class="card-footer clearfix">
                {{ $colors->links() }}
            </div>
        </div>
    </section>
    @include('admin.includes.delete-modal',[
            'message' => __('messages.colors.onDeleteRecord')
        ])
@endsection
