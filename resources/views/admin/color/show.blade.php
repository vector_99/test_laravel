@extends('layouts.app')

@section('content')
    <section class="content-header">
        @include('admin.includes.headerWithBreadcrumbs', [
            'route' => route('color.index'),
            'title' => $title,
            'pageName' => $pageName ?? null
        ])
    </section>
    <section class="content">
        <div class="card">
            <div class="card-header">
                <a class="btn btn-success btn-sm mw-100" href="{{ route('color.edit', $color->id) }}">
                    <i class="fas fa-pencil-alt"></i>
                </a>
            </div>
            <div class="card-body">
                <div class="form-group">
                    <label>Brand name</label>
                    <h3>{{ $color->name }}</h3>
                </div>
            </div>
        </div>
    </section>
@endsection
