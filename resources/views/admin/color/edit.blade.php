@extends('layouts.app')

@section('content')
    <section class="content-header">
        @include('admin.includes.headerWithBreadcrumbs', [
            'route' => route('color.index'),
            'title' => $title,
            'pageName' => $pageName ?? null
        ])
    </section>
    <section class="content">
        @if ($errors->any())
            @include('admin.includes.errors', ['errors' => $errors])
        @endif

        @include('admin/brand/_form', [
            'route' => route('color.update', $color->id),
            'method' => 'PUT',
            'data' => $color
        ])
    </section>
@endsection
