<div class="card card-default">
    <form action="{{ $route }}" method="POST" enctype="multipart/form-data">
        @csrf
        @if($method)
        @method($method)
        @endif
        <div class="card-body">
            <div class="form-group">
                <label for="name">Color name</label>
                <input type="text" class="form-control" id="name" name="name" value="{{ $data->name ?? old('name') }}"
                       placeholder="Enter color name"/>
            </div>
        </div>
        <div class="card-footer">
            <button type="submit" class="btn btn-success">Submit</button>
        </div>
    </form>
</div>
