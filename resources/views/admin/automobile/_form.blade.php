<div class="card card-default">
    <form action="{{ $route }}" method="POST" enctype="multipart/form-data">
        @csrf
        @if($method)
            @method($method)
        @endif
        <div class="card-body">
            <div class="row">
                <div class="col-sm-6">
                    <div class="form-group">
                        <label for="model_id">Model name</label>
                        <select class="custom-select form-control-border" id="model_id" name="model_id"
                                autocomplete="off">
                            <option value="0">
                                Select model
                            </option>
                            @foreach($data['models'] AS $model)
                                <option value="{{ $model->id }}"
                                @if($data['car'])
                                    {{ ($model->id == $data['car']->model_id) ? 'selected=selected' : '' }}
                                    @else
                                    {{ ($model->id == old('model_id')) ? 'selected=selected' : '' }}
                                    @endif
                                >
                                    {{ $model->brand->name . " " . $model->name }}
                                </option>
                            @endforeach
                        </select>
                    </div>

                    <div class="form-group">
                        <label for="transmission">Transmission</label>
                        <select class="custom-select form-control-border" id="transmission" name="transmission"
                                autocomplete="off">
                            <option value="0">
                                Select transmission
                            </option>
                            @foreach($data['transmissions'] AS $transmission)
                                <option value="{{ $transmission }}"
                                @if($data['car'])
                                    {{ ($transmission == $data['car']->transmission) ? 'selected=selected' : '' }}
                                    @else
                                    {{ ($transmission == old('transmission')) ? 'selected=selected' : '' }}
                                    @endif
                                >
                                    {{ $transmission }}
                                </option>
                            @endforeach
                        </select>
                    </div>

                    <div class="form-group">
                        <label for="colorId">Color</label>
                        <select class="custom-select form-control-border" id="colorId" name="color_id"
                                autocomplete="off">
                            <option value="0">
                                Select color
                            </option>
                            @foreach($data['colors'] AS $color)
                                <option value="{{ $color->id }}"
                                @if($data['car'])
                                    {{ ($color->id == $data['car']->color_id) ? 'selected=selected' : '' }}
                                @else
                                    {{ ($color->id == old('color_id')) ? 'selected=selected' : '' }}
                                @endif
                                >
                                    {{ $color->name }}
                                </option>
                            @endforeach
                        </select>
                    </div>

                    <div class="form-group">
                        <label for="yearManufacture">Year of manufacture</label>
                        <input type="text" class="form-control" id="yearManufacture" name="year_manufacture"
                               placeholder="Enter year of manufacture"
                               value="{{ $data['car'] ? $data['car']->year_manufacture : old('year_manufacture') }}"
                        >
                    </div>

                    <div class="form-group">
                        <label for="licensePlate">License plate</label>
                        <input type="text" class="form-control" id="licensePlate" name="license_plate"
                               placeholder="Enter license plate"
                               value="{{ $data['car'] ? $data['car']->license_plate : old('license_plate') }}"
                        >
                    </div>

                    <div class="form-group">
                        <label for="rentalPrice">Rental price</label>
                        <input type="text" class="form-control" id="rentalPrice" name="rental_price"
                               placeholder="Enter rental price"
                               value="{{ $data['car'] ? $data['car']->rental_price : old('rental_price') }}"
                        >
                    </div>
                </div>

                <div class="col-sm-6">
                    @if (isset($data['car']))
                        @if (\Illuminate\Support\Facades\Storage::disk('public')->exists($data['car']->photo))
                            <a type="button" class="modal-image-trigger" data-toggle="modal"
                               data-target="#modal_image"
                               data-image-url="{{ url(\Illuminate\Support\Facades\Storage::url($data['car']->photo)) }}"
                               data-image-alt="{{ $data['car']->model->brand->name }} {{ $data['car']->model->name }}"
                            >
                                <img class="img-fluid"
                                     style="width: 380px;"
                                     src="{{ url(\Illuminate\Support\Facades\Storage::url($data['car']->photo)) }}"
                                     alt="{{ $data['car']->model->brand->name }} {{ $data['car']->model->name }}">
                            </a>

                        @else
                            <img class="img-fluid"
                                 src="https://via.placeholder.com/380.png?text={{ $data['car']->model->brand->name }} {{ $data['car']->model->name }}"
                                 alt="{{ $data['car']->model->brand->name }} {{ $data['car']->model->name }}"/>
                        @endif
                    @else
                        <img class="img-fluid"
                             src="https://via.placeholder.com/380.png?text=Upload car photo"
                             alt="Upload car photo"/>
                    @endif
                    <div class="form-group">
                        <label for="car_photo">Automobile photo (max size - 1Mb)</label>
                        <div class="input-group">
                            <div class="custom-file">
                                <input type="file" class="custom-file-input" id="car_photo" name="car_photo">
                                <label class="custom-file-label" for="exampleInputFile">Choose file</label>
                            </div>
                            <div class="input-group-append">
                                <span class="input-group-text">Upload</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="card-footer">
            <button type="submit" class="btn btn-success">Submit</button>
        </div>
    </form>
</div>
@include('admin.includes.image-modal')
