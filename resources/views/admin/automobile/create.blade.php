@extends('layouts.app')

@section('content')
    <section class="content-header">
        @include('admin.includes.headerWithBreadcrumbs', [
            'route' => route('brand.index'),
            'title' => $title,
            'pageName' => $pageName ?? null
        ])
    </section>
    <section class="content">
        @if ($errors->any())
            @include('admin.includes.errors', ['errors' => $errors])
        @endif

        @include('admin/automobile/_form', [
            'route' => route('car.store'),
            'method' => null,
            'errors' => $errors,
            'data' => $data
        ])
    </section>
@endsection
