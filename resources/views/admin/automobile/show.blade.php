@extends('layouts.app')

@section('content')
    <section class="content-header">
        @include('admin.includes.headerWithBreadcrumbs', [
            'route' => route('car.index'),
            'title' => $title,
            'pageName' => $pageName ?? null
        ])
    </section>
    <section class="content">
        <div class="card">
            <div class="card-header">
                <a class="btn btn-success btn-sm mw-100" href="{{ route('car.edit', $car->id) }}">
                    <i class="fas fa-pencil-alt"></i>
                </a>
            </div>
            <div class="card-body">
                <div class="row">
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label>Model name</label>
                            <p>{{ $car->model->brand->name . " " . $car->model->name }}</p>
                        </div>

                        <div class="form-group">
                            <label>Transmission</label>
                            <p>{{ $car->transmission }}</p>
                        </div>

                        <div class="form-group">
                            <label>Color</label>
                            <p>{{ $car->color->name }}</p>
                        </div>

                        <div class="form-group">
                            <label>Year of manufacture</label>
                            <p>{{ $car->year_manufacture }}</p>
                        </div>

                        <div class="form-group">
                            <label>License plate</label>
                            <p>{{ \Illuminate\Support\Str::upper($car->license_plate) }}</p>
                        </div>

                        <div class="form-group">
                            <label>Rental price</label>
                            <p>{{ $car->rental_price }}</p>
                        </div>
                    </div>

                    <div class="col-sm-6">
                        @if (\Illuminate\Support\Facades\Storage::disk('public')->exists($car->photo))
                            <a type="button" class="modal-image-trigger" data-toggle="modal"
                               data-target="#modal_image"
                               data-image-url="{{ url(\Illuminate\Support\Facades\Storage::url($car->photo)) }}"
                               data-image-alt="{{ $car->model->brand->name }} {{ $car->model->name }}"
                            >
                                <img class="img-fluid"
                                     style="width: 380px;"
                                     src="{{ url(\Illuminate\Support\Facades\Storage::url($car->photo)) }}"
                                     alt="{{ $car->model->brand->name }} {{ $car->model->name }}">
                            </a>
                        @else
                            <img class="img-fluid"
                                 src="https://via.placeholder.com/380.png?text={{ $car->model->brand->name }} {{ $car->model->name }}"
                                 alt="{{ $car->model->brand->name }} {{ $car->model->name }}"/>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </section>
    @include('admin.includes.image-modal')
@endsection
