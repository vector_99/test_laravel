@extends('layouts.app')

@section('content')
    <section class="content-header">
        @include('admin.includes.headerWithBreadcrumbs', [
            'route' => route('brand.index'),
            'title' => $title,
            'pageName' => $pageName ?? null
        ])
    </section>

    <section class="content">
        @if(session()->has('success'))
            @include('admin.includes.success')
        @endif
        <div class="card">
            <div class="card-header">
                <a class="btn btn-success btn-sm mw-100" href="{{ route('car.create') }}">
                    <i class="far fa-plus-square"></i>
                </a>
                <div class="card-tools">
                    {{ $cars->links() }}
                </div>
            </div>
            @if ($cars->isNotEmpty())
                <div class="card-body p-0" style="display: block;">
                <table class="table table-striped projects">
                    <thead>
                    <tr>
                        <th style="width: 1%">#</th>
                        <th style="width: 5%">Photo</th>
                        <th style="width: 10%">Brand name</th>
                        <th style="width: 10%">Model Name</th>
                        <th style="width: 10%">Year</th>
                        <th style="width: 10%">License plate</th>
                        <th style="width: 10%">Color</th>
                        <th style="width: 10%">Transmission</th>
                        <th style="width: 10%">Rental price</th>
                        <th style="width: 5%">{{-- --}}</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach ($cars AS $car)
                    <tr>
                        <td>
                            {{ $car->id }}
                        </td>
                        <td>
                            <a href="{{ route('car.show', $car->id) }}">
                                @if (\Illuminate\Support\Facades\Storage::disk('public')->exists($car->photo))
                                    <img class="img-fluid"
                                         src="{{ url(\Illuminate\Support\Facades\Storage::url($car->photo)) }}"
                                         alt="{{ $car->model->brand->name }} {{ $car->model->name }}">
                                @else
                                    <img class="img-fluid"
                                         src="https://via.placeholder.com/120.png?text={{ $car->model->brand->name }} {{ $car->model->name }}"
                                         alt="{{ $car->model->brand->name }} {{ $car->model->name }}"/>
                                @endif
                            </a>
                        </td>
                        <td>
                            <a href="{{ route('car.show', $car->id) }}">
                                {{ $car->model->brand->name }}
                            </a>
                        </td>
                        <td>
                            <a href="{{ route('car.show', $car->id) }}">
                                {{ $car->model->name }}
                            </a>
                        </td>
                        <td>
                            <a href="{{ route('car.show', $car->id) }}">
                                {{ $car->year_manufacture }}
                            </a>
                        </td>
                        <td>
                            <a href="{{ route('car.show', $car->id) }}">
                                {{ \Illuminate\Support\Str::upper($car->license_plate) }}
                            </a>
                        </td>
                        <td>
                            <a href="{{ route('car.show', $car->id) }}">
                                {{ $car->color->name }}
                            </a>
                        </td>
                        <td>
                            <a href="{{ route('car.show', $car->id) }}">
                                {{ $car->transmission }}
                            </a>
                        </td>
                        <td>
                            <a href="{{ route('car.show', $car->id) }}">
                                {{ $car->rental_price }}
                            </a>
                        </td>
                        <td class="project-actions text-right">
                            <div class="btn-group">
                                <a class="btn btn-outline-primary btn-sm" href="{{ route('car.edit', $car->id) }}">
                                    <i class="fas fa-pencil-alt pt-2 pl-1 pr-1"></i>
                                </a>
                                <a type="button" class="btn btn-outline-danger delete-trigger" data-toggle="modal"
                                   data-target="#modal_delete"
                                   data-delete-url="{{ route('car.destroy', $car->id) }}">
                                    <i class="fas fa-trash"></i>
                                </a>
                            </div>
                        </td>
                    </tr>
                    @endforeach
                    </tbody>
                </table>
                </div>
            @else
                <div class="card-body" style="display: block;">
                    <div class="col-12">
                        <h5>{{ __('messages.cars.noDataFound') }}</h5>
                    </div>
                </div>
            @endif
            <div class="card-footer clearfix">
                {{ $cars->links() }}
            </div>
        </div>
    </section>
    @include('admin.includes.delete-modal',[
            'message' => __('messages.cars.onDeleteRecord')
        ])
@endsection
