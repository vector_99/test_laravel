<div class="container-fluid">
    <div class="row mb-2">
        <div class="col-sm-6">
            <h1>{{ $pageName ?? $title }}</h1>
        </div>
        <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
                <li class="breadcrumb-item">
                    <a href="{{ route('adminHome') }}">Home</a>
                </li>
                @if($title)
                    <li class="breadcrumb-item {{ !$pageName ?? 'active' }}">
                        <a href="{{ $route }}">{{ $title }}</a>
                    </li>
                @endif
                @if(isset($pageName))
                    <li class="breadcrumb-item active">{{ $pageName }}</li>
                @endif
            </ol>
        </div>
    </div>
</div>

