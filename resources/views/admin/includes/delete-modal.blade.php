<div id="modal_delete" class="modal fade show modal_delete" id="modal-danger" aria-modal="true" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header bg-danger">
                <h4 class="modal-title">
                    <i class="fa fa-question-circle fa-fw mr-1 text-white"></i>
                    {{ __('messages.modals.delete.title') }}
                </h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">
                <p>{!! $message !!}</p>
            </div>
            <div class="modal-footer justify-content-between">
                <button type="button" class="btn btn-default" data-dismiss="modal">{{ __('messages.modals.delete.cancel') }}</button>
                <form id="modal_delete_form" action="" method="POST">
                    @csrf
                    @method('DELETE')
                    <button type="submit" class="btn btn-danger">
                        <i class="fas fa-trash"></i>
                        {{ __('messages.modals.delete.confirm') }}
                    </button>
                </form>
            </div>
        </div>
    </div>
</div>

@push('page_scripts')
    <script type="text/javascript">
        jQuery('.delete-trigger').click(function () {
            let deleteUrl = $(this).data("deleteUrl");
            jQuery('#modal_delete').on('show.bs.modal', function () {
                jQuery('#modal_delete_form').attr("action", deleteUrl)
            });
        });
    </script>
@endpush
