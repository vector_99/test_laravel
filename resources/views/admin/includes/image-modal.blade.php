<div id="modal_image" class="modal fade show" aria-modal="true" role="dialog">
    <div class="modal-dialog modal-dialog modal-xl">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">
                    <i class="fas fa-image"></i>
                    <span id="title_text"></span>
                </h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-sm-12 text-center">
                    <img id="image" class="img-fluid " src="" />
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@push('page_scripts')
    <script type="text/javascript">
        jQuery('.modal-image-trigger').click(function () {
            let imageUrl = $(this).data("imageUrl");
            let imageAlt = $(this).data("imageAlt");
            jQuery('#modal_image').on('show.bs.modal', function () {
                console.log(imageAlt);
                jQuery('#image').attr('src', imageUrl);
                jQuery('#image').attr('alt', imageAlt);
                jQuery('#image').attr('title', imageAlt);
                jQuery('#title_text').html(imageAlt)
            });
        });
    </script>
@endpush
