<div class="card card-default">
    <form action="{{ $route }}" method="POST" enctype="multipart/form-data">
        @csrf
        @if($method)
        @method($method)
        @endif
        <div class="card-body">
            <div class="form-group">
                <label for="brand_id">Brand name</label>
                <select class="custom-select form-control-border" id="brand_id" name="brand_id" autocomplete="off">
                    <option value="0">
                        Select brand name
                    </option>
                    @foreach($data['brands'] AS $brand)
                        <option value="{{ $brand->id }}"
                            @if($data['model'])
                                {{ ($brand->id == $data['model']->brand_id) ? 'selected=selected' : '' }}
                            @else
                                {{ ($brand->id == old('brand_id')) ? 'selected=selected' : '' }}
                            @endif
                            >
                            {{ $brand->name }}
                        </option>
                    @endforeach
                </select>
            </div>
            <div class="form-group">
                <label for="name">Model name</label>
                <input type="text" class="form-control" id="name" name="name" value="{{ $data['model']->name ?? old('name') }}"
                       placeholder="Enter model name"/>
            </div>
        </div>
        <div class="card-footer">
            <button type="submit" class="btn btn-success">Submit</button>
        </div>
    </form>
</div>
