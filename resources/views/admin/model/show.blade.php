@extends('layouts.app')

@section('content')
    <section class="content-header">
        @include('admin.includes.headerWithBreadcrumbs', [
            'route' => route('model.index'),
            'title' => $title,
            'pageName' => $pageName ?? null
        ])
    </section>
    <section class="content">
        <div class="card">
            <div class="card-header">
                <a class="btn btn-success btn-sm mw-100" href="{{ route('model.edit', $model->id) }}">
                    <i class="fas fa-pencil-alt"></i>
                </a>
            </div>
            <div class="card-body">
                <div class="form-group">
                    <label for="exampleInputEmail1">Brand name</label>
                    <h3>{{ $model->brand->name }}</h3>
                </div>
                <div class="form-group">
                    <label for="exampleInputEmail1">Model name</label>
                    <h3>{{ $model->name }}</h3>
                </div>
            </div>
        </div>

    </section>
@endsection
