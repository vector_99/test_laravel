@extends('layouts.app')

@section('content')
    <section class="content-header">
        @include('admin.includes.headerWithBreadcrumbs', [
            'route' => route('model.index'),
            'title' => $title,
            'pageName' => $pageName ?? null
        ])
    </section>
    <section class="content">
        @if ($errors->any())
            @include('admin.includes.errors', ['errors' => $errors])
        @endif

        @include('admin/model/_form', [
            'route' => route('model.update', $data['model']->id),
            'method' => 'PUT',
            'errors' => $errors,
            'data' => $data
        ])
    </section>
@endsection
