@extends('layouts.app')

@section('content')
    <section class="content-header">
        @include('admin.includes.headerWithBreadcrumbs', [
            'route' => route('model.index'),
            'title' => $title,
            'pageName' => $pageName ?? null
        ])
    </section>

    <section class="content">
        @if(session()->has('success'))
            @include('admin.includes.success')
        @endif
        <div class="card">
            <div class="card-header">
                <a class="btn btn-success btn-sm mw-100" href="{{ route('model.create') }}">
                    <i class="far fa-plus-square"></i>
                </a>
                <div class="card-tools">
                    {{ $models->links() }}
                </div>
            </div>
            @if ($models->isNotEmpty())
            <div class="card-body p-0" style="display: block;">
                <table class="table table-striped projects">
                    <thead>
                    <tr>
                        <th style="width: 1%">
                            #
                        </th>
                        <th style="width: 40%">
                            Model Name
                        </th>
                        <th style="width: 40%">
                            Brand Name
                        </th>
                        <th style="width: 20%">
                        </th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($models AS $model)
                    <tr>
                        <td>
                            {{ $model->id }}
                        </td>
                        <td>
                            <a href="{{ route('model.show', $model->id) }}">
                                {{ $model->name }}
                            </a>
                            <br>
                        </td>
                        <td>
                            <a href="{{ route('brand.show', $model->brand->id) }}">
                                {{ $model->brand->name }}
                            </a>
                        </td>
                        <td class="project-actions text-right">
                            <div class="btn-group">
                                <a class="btn btn-outline-primary btn-sm" href="{{ route('model.edit', $model->id) }}">
                                    <i class="fas fa-pencil-alt pt-2 pl-1 pr-1"></i>
                                </a>
                                <a type="button" class="btn btn-outline-danger delete-trigger" data-toggle="modal"
                                        data-target="#modal_delete"
                                        data-delete-url="{{ route('model.destroy', $model->id) }}">
                                    <i class="fas fa-trash"></i>
                                </a>
                            </div>
                        </td>
                    </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
            @else
                <div class="card-body" style="display: block;">
                    <div class="col-12">
                        <h5>{{ __('messages.models.noDataFound') }}</h5>
                    </div>
                </div>
            @endif
            <div class="card-footer clearfix">
                {{ $models->links() }}
            </div>
        </div>
    </section>
    @include('admin.includes.delete-modal',[
            'message' => __('messages.models.onDeleteRecord')
        ])
@endsection
