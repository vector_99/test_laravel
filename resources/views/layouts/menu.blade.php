<!-- need to remove -->
<li class="nav-item">
    <a href="{{ route('adminHome') }}" class="nav-link {{ (request()->is('admin')) ? 'active' : '' }}">
        <i class="nav-icon fas fa-home"></i>
        <p>Home</p>
    </a>
</li>

<li class="nav-header">{{ \Illuminate\Support\Str::upper('Catalog') }}</li>
<li class="nav-item">
    <a href="{{ route('car.index') }}" class="nav-link {{ (request()->is('admin/car*')) ? 'active' : '' }}">
        <i class="nav-icon fas fa-car"></i>
        <p>Automobile</p>
    </a>
</li>

<li class="nav-header">{{ \Illuminate\Support\Str::upper('Reference Book') }}</li>
<li class="nav-item">
    <a href="{{ route('brand.index') }}" class="nav-link {{ (request()->is('admin/brand*')) ? 'active' : '' }}">
        <i class="nav-icon fas fa-city"></i>
        <p>Brand</p>
    </a>
</li>
<li class="nav-item">
    <a href="{{ route('model.index') }}" class="nav-link {{ (request()->is('admin/model*')) ? 'active' : '' }}">
        <i class="nav-icon fas fa-trademark"></i>
        <p>Model</p>
    </a>
</li>
<li class="nav-item">
    <a href="{{ route('color.index') }}" class="nav-link {{ (request()->is('admin/color*')) ? 'active' : '' }}">
        <i class="nav-icon fas fa-paint-brush"></i>
        <p>Colors</p>
    </a>
</li>

<li class="nav-header"><hr class="btn-default mt-1 mb-1"/></li>

<li class="nav-item">
    <a href="#" class="nav-link"
       onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
        <i class="nav-icon fas fa-door-open"></i>
        <p>Sign out</p>
    </a>
    <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
        @csrf
    </form>
</li>

