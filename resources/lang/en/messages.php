<?php

return [
    'brands' => [
        'noDataFound' => 'Sorry. No brands yet :(',
        'onDeleteRecord' => 'Are you sure you want to delete this? This action will
<strong>delete all related entries</strong> in the <strong>cars and models table</strong>!',
    ],
    'models' => [
        'noDataFound' => 'Sorry. No models yet :(',
        'onDeleteRecord' => 'Are you sure you want to delete this?<br/>
This action will <strong>delete all related entries</strong> in the <strong>cars table</strong>!',
    ],
    'cars' => [
        'noDataFound' => 'Sorry. No cars yet :(',
        'onDeleteRecord' => 'Are you sure you want to delete this?',
    ],
    'colors' => [
        'noDataFound' => 'Sorry. No colors yet :(',
        'onDeleteRecord' => 'Are you sure you want to delete this?',
    ],
    'modals' => [
        'delete' => [
            'title'     => 'Confirm Delete',
            'cancel'    => 'Cancel',
            'confirm'   => 'Confirm Delete',
        ]
    ],
];
