## Autocatalog

### Prerequisites

The application was developed and tested in the following environment:

Apache 2.4, PHP 7.4.3, MySQL Mariadb 10.5

**Installed PHP modules:**
calendar, ctype, curl, date, dom, exif, FFI, fileinfo, filter, ftp, gettext, hash, iconv, json, libxml, mbstring, mysqli, mysqlnd, openssl, pcntl, pcre, PDO, pdo_mysql, pdo_sqlite, Phar, posix, readline, session, shmop, SimpleXML, sodium, SPL, sqlite3, standard, sysvmsg, sysvsem, sysvshm, tokenizer, xml, xmlreader, xmlwriter, xsl, Zend OPcache, zlib

### Instalation

1. Make project folder.

```bash
mkdir /you/project/name
```

2. Cloning project from [GIT](https://bitbucket.org/vector_99/test_laravel/src/master/) to you folder:

```bash
cd /you/project/name
git clone git@bitbucket.org:vector_99/test_laravel.git .
```

3. Create an empty database

4. Make a copy of the .env.example file in .env

```bash
cp .env.example .env
```

5. Set up a database connection. Open the .env file and edit it as follows

```dotenv
DB_CONNECTION=mysql
DB_HOST=YouDBHost
DB_PORT=3306
DB_DATABASE=YouDBName
DB_USERNAME=YouDBUserName
DB_PASSWORD=YouDBUserPassword
```

6. Install composer dependencies

```bash
composer install
```

7. Install NPM dependencies and build them 

```bash
npm install && npm run dev
```

8. Apply the migration to create the required table in the database

```bash
php artisan migrate
```

9. Fill in the fields with fake data

```bash
php artisan db:seed
```

10. Make you APP key

```bash
php artisan key:generate
```

11. Start the project

```bash
php artisan serve
```

12. Open a browser and enter the address, usually it is the address 127.0.0.1:8000 (or the one on which Artisan Serve will start)

---

### How it use 

1. Authorization window

<p align="center"><img src="https://bitbucket.org/vector_99/test_laravel/downloads/autoCatalogLoginPage.png" width="600"></p>

2. Login details
   
admin **John Connor**:

```dotenv
    login: john-connor@gmail.com
    password: p@ssworD
``` 

user **Sarah Connor**: 

```dotenv
    login: sarah-connor@gmail.com
    password: p@ssworD
```

3. After authorization with the administrator role, you will be taken to the catalog administration page:

<p align="center"><img src="https://bitbucket.org/vector_99/test_laravel/downloads/autoCatalogMainPage.png" width="600"></p>

 - The **REFERENCE BOOK** section contains reference books of Models, brands and colors of cars

 - The **CATALOG** section contains a catalog of cars

The database is already filled with several cars, models and brands. In addition, there are several colors of cars.
You can easily delete all data and add your real data.
